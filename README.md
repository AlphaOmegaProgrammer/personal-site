This is my personal website. Remember that everything on the Internet is 100% serious and should be taken seriously.



## Build Instructions

I'm still working on this interface, but I've created a Static-Site Generator (SSG) for  building the HTML for this website.

The SSG code is entirely in BASH, and just simply concatenates files together with minimal processing.

This website can be built with the `make` command or by running the `build.sh` script directly. You delete the generated files with `make clean`. All of the generated files will appear in a directory named `public`.



## Licensing

I'm not a lawyer and don't understand the intricacies of licensing. If I did something wrong with the licensing, talk to me about it.

This repo is licensed under CC0, however there are a handful of notable 3rd party things I've included that have open licenses, but are not liscened under CC0.

These things are:

 * The font [Chakra Petch](https://fonts.google.com/specimen/Chakra+Petch/about), which uses the OFL-1.1 license.
 * The font [Crimson Pro](https://github.com/Fonthausen/CrimsonPro/tree/master), which uses the OFL-1.1 license.
