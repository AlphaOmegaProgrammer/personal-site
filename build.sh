#!/bin/bash

BUILD_LOCAL=0;
INTEGRITY_ATTRIBUTES=$((1 - ${BUILD_LOCAL}));

ROOT_DIR=$(realpath $(dirname ${0}));

if [ ${BUILD_LOCAL} == 1 ];
then
	DOMAIN="file://${ROOT_DIR}/public";
else
	DOMAIN="https://catgirls.meme";
fi;


PRIMARY_NAME="AOP's Domain";
PRIMARY_DOMAIN="catgirls.meme";
TITLE_DELIMITER="|";

TIMESTAMP=$(TZ="UTC" date "+%D %T UTC");
DATESTAMP=$(TZ="UTC" date "+%F");
VERSION=$(echo ${TIMESTAMP} | md5sum | cut -d\  -f1);

PAGES=$(find ${ROOT_DIR}/pages/ -type d);


mkdir -p ${ROOT_DIR}/public/static;

echo -en "User-Agent: *\nsitemap: ${DOMAIN}/sitemap.xml\n" > ${ROOT_DIR}/public/robots.txt;
echo -en "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<urlset xmlns=\"http://www.sitemaps.org/schemas/sitemap/0.9\">\n" > ${ROOT_DIR}/public/sitemap.xml;

mkdir -p ${ROOT_DIR}/public/static/
cp -R ${ROOT_DIR}/static/*/ ${ROOT_DIR}/public/static/

for F in $(find ${ROOT_DIR}/static -type f);
do
	cat ${F} | sed "s#{ROOT}#${DOMAIN}/#" > ${ROOT_DIR}/public/static/$(basename ${F});
done;

for P in ${PAGES};
do
	TEMPLATE='default';
	if [ -e ${P}/template ];
	then
		TEMPLATE=$(cat ${P}/template);
	fi;

	ADD_TO_SITEMAP=1;

	case "${TEMPLATE}" in
		"default")
			TARGET_DIR=${P/pages/public};
			URL=$(echo ${P} | grep -o /pages/.* | cut -d/ -f3-);
			TITLE=${PRIMARY_NAME};

			if [ "${URL}" != "" ];
			then
				URL=${URL}"/";
			fi;

			if [ -e ${P}/title ];
			then
				TITLE=$(cat ${P}/title)" ${TITLE_DELIMITER} ${TITLE}";
			fi;

			# First, create the directory for index.html so we don't deal with URL rewriting
			mkdir -p ${TARGET_DIR};

			# Now that we've generated all of the data, start the template
			FONTS_CSS_LINK="";
			THEME_CSS_LINK="";
			SYNTAX_HIGHLIGHTING_CSS_LINK="";

			# Apparently file integrity just doesn't work for local builds...
			if [ $INTEGRITY_ATTRIBUTES == 1 ];
			then
				FONTS_CSS_HASH=$(sha512sum ${ROOT_DIR}/public/static/fonts.css | cut -d\  -f1 | xxd -r -p | base64 | tr -d "\n");
				THEME_CSS_HASH=$(sha512sum ${ROOT_DIR}/public/static/theme.css | cut -d\  -f1 | xxd -r -p | base64 | tr -d "\n");
				SYNTAX_CSS_HASH=$(sha512sum ${ROOT_DIR}/public/static/syntax-highlighting.css | cut -d\  -f1 | xxd -r -p | base64 | tr -d "\n");
			
				FONTS_CSS_LINK="<link type=\"text/css\" rel=\"stylesheet\" href=\"${DOMAIN}/static/fonts.css?v=${VERSION}\" integrity=\"sha512-${FONTS_CSS_HASH}\">";
				THEME_CSS_LINK="<link type=\"text/css\" rel=\"stylesheet\" href=\"${DOMAIN}/static/theme.css?v=${VERSION}\" integrity=\"sha512-${THEME_CSS_HASH}\">";
				SYNTAX_CSS_LINK="<link type=\"text/css\" rel=\"stylesheet\" href=\"${DOMAIN}/static/syntax-highlighting.css?v=${VERSION}\" integrity=\"sha512-${SYNTAX_CSS_HASH}\">";
			else
				FONTS_CSS_LINK="<link type=\"text/css\" rel=\"stylesheet\" href=\"${DOMAIN}/static/fonts.css?v=${VERSION}\">";
				THEME_CSS_LINK="<link type=\"text/css\" rel=\"stylesheet\" href=\"${DOMAIN}/static/theme.css?v=${VERSION}\">";
				SYNTAX_CSS_LINK="<link type=\"text/css\" rel=\"stylesheet\" href=\"${DOMAIN}/static/syntax-highlighting.css?v=${VERSION}\">";
			fi;

			echo -e "<!DOCTYPE html>\n"\
				"<html lang="en">"\
				"\n\t<head>"\
				"\n\t\t<meta charset=\"utf-8\">"\
				"\n\t\t<title>${TITLE}</title>"\
				"\n\t\t<meta property=\"og:locale\" content=\"en_US\">"\
				"\n\t\t<meta property=\"og:site_name\" content=\"${PRIMARY_NAME}\">"\
				"\n\t\t<meta property=\"og:title\" content=\"${TITLE}\">"\
				"\n\t\t<meta property=\"og:url\" content=\"${DOMAIN}/${URL}\">"\
				"\n\t\t<link rel="canonical" href=\"${DOMAIN}/${URL}\">"\
				"\n\t\t<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">"\
				"\n\t\t${FONTS_CSS_LINK}"\
				"\n\t\t${THEME_CSS_LINK}"\
				"\n\t\t${SYNTAX_CSS_LINK}"\
				> ${TARGET_DIR}/index.html;

			# If there HTML head stuff, add it
			if [ -e ${P}/html-head.html ];
			then
				cat ${P}/html-head.html | sed -e 's/^/\t\t/' -e "s#{ROOT}#${DOMAIN}/#" -e "s#{VERSION}#${VERSION}#" >> ${TARGET_DIR}/index.html;
			fi;

			# End head, start body
			echo -e "\t</head>"\
				"\n\t<body>"\
				"\n\t\t<div id=\"page_container\">"\
				"\n\t\t\t<div id=\"page\">"\
				"\n\t\t\t\t<div id=\"header_container\">"\
				"\n\t\t\t\t\t<header>"\
				"\n\t\t\t\t\t\t<nav id=\"main_nav\">"\
				"\n\t\t\t\t\t\t\t<a href=\"${DOMAIN}/\">Home</a>"\
				"\n\t\t\t\t\t\t\t<a href=\"${DOMAIN}/catgirls/\">Catgirls</a>"\
				"\n\t\t\t\t\t\t\t<a href=\"${DOMAIN}/rants/\">Rants</a>"\
				"\n\t\t\t\t\t\t\t<a href=\"${DOMAIN}/books/\">Books</a>"\
				"\n\t\t\t\t\t\t</nav>"\
				"\n\t\t\t\t\t</header>"\
				"\n\t\t\t\t</div>"\
				"\n\t\t\t\t<div id=\"body_container\">"\
				"\n\t\t\t\t\t<main>"\
				>> ${TARGET_DIR}/index.html;

			case "${P}" in
				"${ROOT_DIR}/pages/rants/"*)
					echo -e "\t\t\t\t\t\t<h1 class=\"page_title\">$(cat ${P}/title)</h1>" >> ${TARGET_DIR}/index.html;
					cat ${P}/body.html | sed 's/^/\t\t\t\t\t\t/' | sed "s#{ROOT}#${DOMAIN}/#" >> ${TARGET_DIR}/index.html;
					;;

				"${ROOT_DIR}/pages/books/"*"/"*)
					ADD_TO_SITEMAP=0;
					# Do nothing, since these directories are handled in their parents' directory
					;;

				"${ROOT_DIR}/pages/books/"*)
					# Get section dirs in order
					TABLE_OF_CONTENTS="\t\t\t\t\t\t\t<h2 class=\"table_of_contents_heading\">Table Of Contents</h2>\n\t\t\t\t\t\t\t<div class=\"table_of_contents\">";
					SECTIONS="";

					for SECTION_NUMBER in $(find ${P} -mindepth 1 -maxdepth 1 -type d | rev | cut -d/ -f1 | rev | cut -d_ -f1 | sort -V);
					do
						SECTION=$(ls ${P} | grep "^${SECTION_NUMBER}_");
						TABLE_OF_CONTENTS=${TABLE_OF_CONTENTS}"\n\t\t\t\t\t\t\t\t<div class=\"table_of_contents_item table_of_contents_item_level_$(echo ${SECTION} | tr -cd '.' | wc -c)\">${SECTION_NUMBER} <a href=\"#${SECTION}\">$(cat ${P}/${SECTION}/title)</a></div>";

						SECTIONS=${SECTIONS}$(
							echo -en "\n\t\t\t\t\t\t\t\t<article id=\"${SECTION}\">"\
								"\n\t\t\t\t\t\t\t\t\t<h2>${SECTION_NUMBER} $(cat ${P}/${SECTION}/title)</h2>"\
								"\n\t\t\t\t\t\t\t\t\t"$(cat ${P}/${SECTION}/body.html | sed 's/^/\t\t\t\t\t\t\t\t\t/' | sed "s#{ROOT}#${DOMAIN}/#")\
								"\n\t\t\t\t\t\t\t\t</article>"\
						);
					done;

					TABLE_OF_CONTENTS=${TABLE_OF_CONTENTS}"\n\t\t\t\t\t\t\t</div>";

					echo -e "\t\t\t\t\t\t<h1 class=\"page_title\">$(cat ${P}/title)</h1>" >> ${TARGET_DIR}/index.html;
					cat ${P}/preface.html | sed 's/^/\t\t\t\t\t\t\t/' | sed "s#{ROOT}#${DOMAIN}/#" >> ${TARGET_DIR}/index.html;
					echo -en "${TABLE_OF_CONTENTS}" >> ${TARGET_DIR}/index.html;
					echo -e "${SECTIONS}" >> ${TARGET_DIR}/index.html;
					cat ${P}/postface.html | sed 's/^/\t\t\t\t\t\t\t/' | sed "s#{ROOT}#${DOMAIN}/#" >> ${TARGET_DIR}/index.html;
					;;


				*)
					# Add the body content
					cat ${P}/body.html | sed 's/^/\t\t\t\t\t\t/' | sed "s#{ROOT}#${DOMAIN}/#" >> ${TARGET_DIR}/index.html;
					;;
			esac;


			# End the file
			echo -e "\t\t\t\t\t</main>"\
				"\n\t\t\t\t</div>"\
				"\n\t\t\t\t<div id=\"footer_container\">"\
				"\n\t\t\t\t\t<footer>"\
				"\n\t\t\t\t\t\t<span id=\"last_generated\">Last Generated: <span>${TIMESTAMP}</span></span>"\
				"\n\t\t\t\t\t\t<span id=\"social_icons\">"\
				"\n\t\t\t\t\t\t\t<a href=\"https://www.twitch.tv/alphaomegaprogrammer\" target=\"_blank\"><img src=\"${DOMAIN}/static/twitch-logo.svg\" alt=\"Twitch\" title=\"Twitch Profile\"></a>"\
				"\n\t\t\t\t\t\t\t<a href=\"https://gitlab.com/AlphaOmegaProgrammer\" target=\"_blank\"><img src=\"${DOMAIN}/static/gitlab-logo.svg\" alt=\"GitLab\" title=\"GitLab Profile\"></a>"\
				"\n\t\t\t\t\t\t\t<a href=\"https://twitter.com/OmegaNekoSimp\" target=\"_blank\"><img src=\"${DOMAIN}/static/x-logo.svg\" alt=\"X\" title=\"X Profile\"></a>"\
				"\n\t\t\t\t\t\t\t<a href=\"https://leetcode.com/AlphaOmegaProgrammer/\" target=\"_blank\"><img src=\"${DOMAIN}/static/leetcode-logo.svg\" alt=\"LeetCode\" title=\"LeetCode Profile\"></a>"\
				"\n\t\t\t\t\t\t\t<a href=\"https://lichess.org/@/AlphaOmegaProgrammer\" target=\"_blank\"><img src=\"${DOMAIN}/static/lichesslogowhite.svg\" alt=\"lichess\" title=\"lichess Profile\"></a>"\
				"\n\t\t\t\t\t\t</span>"\
				"\n\t\t\t\t\t\t<span id="copyleft">"\
				"\n\t\t\t\t\t\t\t<p xmlns:cc=\"http://creativecommons.org/ns#\" >Unless <a href=\"https://gitlab.com/AlphaOmegaProgrammer/personal-site#licensing\" target=\"_blank\">specified otherwise</a>, this work is marked with <a href=\"http://creativecommons.org/publicdomain/zero/1.0?ref=chooser-v1\" target=\"_blank\" rel=\"license noopener noreferrer\" style=\"display:inline-block;\">CC0 1.0<img style=\"height:22px!important;margin-left:3px;vertical-align:text-bottom;\" src=\"https://mirrors.creativecommons.org/presskit/icons/cc.svg?ref=chooser-v1\" alt=\"cc\" title=\"cc\"><img style=\"height:22px!important;margin-left:3px;vertical-align:text-bottom;\" src=\"https://mirrors.creativecommons.org/presskit/icons/zero.svg?ref=chooser-v1\" alt=\"0\" title=\"0\"></a></p>"\
				"\n\t\t\t\t\t\t</span>"\
				"\n\t\t\t\t\t</footer>"\
				"\n\t\t\t\t</div>"\
				"\n\t\t\t</div>"\
				"\n\t\t</div>"\
				"\n\t</body>"\
				"\n</html>"\
				>> ${TARGET_DIR}/index.html;
		;;

		*)
			echo "Unknown template ${TEMPLATE} for page ${DIR}";
			exit 1;
		;;
	esac;

	if [ ${ADD_TO_SITEMAP} == 1 ];
	then
		echo -en "\t<url>\n" \
			"\t\t<loc>${DOMAIN}/${URL}</loc>\n" \
			"\t\t<changefreq>weekly</changefreq>\n" \
			"\t\t<lastmod>${DATESTAMP}</lastmod>\n"\
		"\t</url>\n" >> ${ROOT_DIR}/public/sitemap.xml;
	fi;
done;

echo -en "</urlset>" >> ${ROOT_DIR}/public/sitemap.xml;
