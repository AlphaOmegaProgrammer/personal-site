This file is for giving a special thanks to people who have contributed in some way:

[dfols](https://github.com/dfols) on GitHub - [Fixing alternating background colors for horizontally scrolled code blocks](https://gitlab.com/AlphaOmegaProgrammer/personal-site/-/commit/13ba0620f32340eb626f02bceb8c698e51b4619f)

