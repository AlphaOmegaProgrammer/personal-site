<p><strong>Published on <time datetime="2024-06-10">June 10th, 2024</time></strong></p>
<p>Yes, I'm serious. Let me cook. My ingredients can be found in <a href="https://gitlab.com/AlphaOmegaProgrammer/stop-doing-data-synchronization-reference-repository" target="_blank">this repository</a> (please read the instructions before compiling and executing yourself).</p>
<p>Let's start with a simple idea, make a program that counts from 0 to 1 billion by incrementing a counter 1 billion times as fast as possible. Yes, I know there are many ways to do this instantly, but we're going to use incrementing a counter as a proxy for actually useful and interesting work.</p>
<h2 id="the-control">The Control</h2>
<p>The first step of any experiment is a control, so I wrote a simple control program (<a href="https://gitlab.com/AlphaOmegaProgrammer/stop-doing-data-synchronization-reference-repository/-/blob/master/01-control.c?ref_type=heads" target="_blank">source here</a>).</p>
<p>The program simply increments a counter in a loop until the counter reaches 1 billion. On my machine, I got the following timings:</p>
<blockquote class="codeblock" data-language="stdout">
	<div>real&nbsp;&nbsp;&nbsp;&nbsp;0m2.450s</div>
	<div>user&nbsp;&nbsp;&nbsp;&nbsp;0m2.450s</div>
	<div>sys&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;0m0.000s</div>
</blockquote>
<p>Note we don't care about the exact time, we just want to see generally how strategies perform so we can find patterns.</p>
<p>Anyway, we can see that a single thread simply counting up a loop to 1 billion takes around 2.5 seconds on my CPU. This is the benchmark that we will want to compare future timings against.</p>
<h2 id="mutexes">Mutexes</h2>
<p>Now, let's look at using the most basic of data synchronization primitives: the mutex (<a href="https://gitlab.com/AlphaOmegaProgrammer/stop-doing-data-synchronization-reference-repository/-/blob/master/02-mutex.c?ref_type=heads" target="_blank">source here</a>).</p>
<p>This program spawns 4 threads, and these 4 threads use a mutex to control access to the global counter. Yes, the time for spawning the threads will be included in the timings, but it's negligible.</p>
<blockquote class="codeblock" data-language="stdout">
	<div>real&nbsp;&nbsp;&nbsp;&nbsp;0m2.578s</div>
	<div>user&nbsp;&nbsp;&nbsp;&nbsp;0m3.905s</div>
	<div>sys&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;0m4.987s</div>
</blockquote>
<p>Now this is a bit more interesting. The real time is a little bit higher, but realistically there's not much of a difference. Looking at the user time though, we can see that the threads used almost 50% more CPU time to calculate the same data in the same amount of time, and about twice as much time was spent context switching by the OS (which is just wasted CPU time). This is obviously quite inefficient, but wait there's more!</p>
<p>This program <strong>only counted up to 10 million</strong>, not 1 billion. So using a mutex and 4 threads uses 4 times as many resources and is 100 times slower compared to using a single thread. That's beyond inefficient, that's enterprise!</p>
<h2 id="atomics">Atomics</h2>
<p>Now I'm sure that the more educated among you readers are sitting there and thinking to yourself "Everybody knows that mutexes are slow, atomics are much better and faster!" Well, good sir or madam or whatever pronoun, set your tea cup back on your coaster and watch this (<a href="https://gitlab.com/AlphaOmegaProgrammer/stop-doing-data-synchronization-reference-repository/-/blob/master/03-atomic.c?ref_type=heads" target="_blank">source here</a>).</p>
<p>This program spawns 4 threads, and these 4 threads each implement <a href="https://en.wikipedia.org/wiki/Read-copy-update" target="_blank">RCU</a> by having a local counter and using compare exchange to update the global counter. Yes, I know C has <a href="https://en.cppreference.com/w/c/thread" target="_blank">many other options</a> like <code><a href="https://en.cppreference.com/w/c/atomic/atomic_fetch_add" target="_blank">atomic_fetch_add()</a></code> that are much better for counting atomically, but remember that counting is a proxy for real work, and real work is done using RCU.</p>
<blockquote class="codeblock" data-language="stdout">
	<div>real&nbsp;&nbsp;&nbsp;&nbsp;0m2.676s</div>
	<div>user&nbsp;&nbsp;&nbsp;&nbsp;0m10.417s</div>
	<div>sys&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;0m0.001s</div>
</blockquote>
<p>Looking at the results, we can see that using an atomic compare exchange is slightly slower than the control, but the user time is very close to <code>4 * 2.676s</code>, so that means all 4 threads actually used the CPU pretty efficiently! Also, sys is more or less 0, which means there was very little context switching during execution. That's great, but this is still about the same speed as using a single thread... This is just overhead from doing multithreading, right? We can just add more threads and it'll outpace the single thread, right?</p>
<p><img src="{ROOT}static/images/rants_stop-doing-data-synchronization/right.jpg" alt="meme" title="...right?"></p>
<p>Ok I lied. The atomics test <em>also</em> only counts to 10 million, because 1 billion was also too slow. So just like with mutexes, multiply the timings by 100 to get an accurate comparison to the control.</p>
<h2 id="everything-sucks">Everything Sucks</h2>
<p>We can clearly see that everything sucks and there is no easy answer, so let's do a little bit of thinking and investigating. Using 4 threads with both a mutex and with an atomic operation were both incredibly slow, but why? Let's rerun the previous two tests, but using only a single thread:</p>
<h4 id="mutex-single-thread">Mutex - Single Thread (up to 1 billion)</h4>
<blockquote class="codeblock" data-language="stdout">
	<div>real&nbsp;&nbsp;&nbsp;&nbsp;0m28.922s</div>
	<div>user&nbsp;&nbsp;&nbsp;&nbsp;0m28.922s</div>
	<div>sys&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;0m0.000s</div>
</blockquote>
<h4 id="atomics-single-thread">Atomics - Single Thread (up to 1 billion)</h4>
<blockquote class="codeblock" data-language="stdout">
	<div>real&nbsp;&nbsp;&nbsp;&nbsp;0m15.105s</div>
	<div>user&nbsp;&nbsp;&nbsp;&nbsp;0m15.101s</div>
	<div>sys&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;0m0.002s</div>
</blockquote>
<p>It turns out that atomics are about twice as fast as mutexes, and mutexes provide about an 11x slowdown compared to not doing data synchronization, while atomics have about a 6x slowdown compared to not doing data synchronization. But why does adding more threads make things significantly slower?</p>
<h2 id="why-are-more-threads-slower">Why Are More Threads Slower?</h2>
<p>Contention.</p>
<h2 id="uh-can-you-explain-contention">Uh, Can You Explain Contention?</h2>
<p>Ok fine, let me actually explain what contention is. The entire point of a mutex is to block all threads from executing a critical section without the mutex, and only one thread can hold the mutex at a time... That's just making your program single threaded with extra steps. While that one thread holds the mutex, all of the other threads that are waiting on the mutex basically just <code>sleep()</code>. What's even worse, the OS doesn't even know which thread in your program currently holds the mutex, so if the thread holding the mutex gets context switched out, all of the other threads waiting for that mutex are just deadlocked until that thread context switches back in.</p>
<p>But what about atomics? Atomics also make your program single threaded with extra steps, but at the hardware level and the mutex is built into every atomic operation with the <code>LOCK</code> instruction prefix (section 1.2.5 of <a href="https://www.amd.com/content/dam/amd/en/documents/processor-tech-docs/programmer-references/24594.pdf" target="_blank">this document</a>). This will generally result in higher throughput since atomic locks only last for a single CPU instruction, so threads don't end up blocking each other. But why did atomics perform a little slower than a mutex with 4 threads, and then perform almost twice as fast as a mutex with a single thread? Well, think about how atomics work.</p>
<p>With a mutex, a thread waits on the mutex. Once the thread gets the mutex, the thread does its work, then the thread releases the mutex. Pretty straight-forward, the thread isn't doing any meaningful amount of extra work. Compare this to atomics, which usually use RCU. Every time the compare exchange fails, the thread has to prepare the data again so it can attempt the compare exchange again. Not to mention, looping around this behavior is unpredictable, and will likely cause lots of branch prediction misses.</p>
<p>All of this extra work is much more expensive than just waiting, and all of this extra work simply means that possible chances to make forward progress are missed. This is why there is a common wisdom that atomics are faster than mutexes, but mutexes are faster than atomics under heavy contention.</p>
<h2 id="can-threading-be-fast">Can Threading Even Be Fast?</h2>
<p>Fortunately, the answer is yes! Just don't do data synchronization! It really is that easy. Oh, what's that? Race conditions? Right, it's time for me to explain.</p>
<p>What I mean by "don't do data synchronization" is "architect your program in a way such that you minimize race conditions and therefore reduce contention". I bet you wouldn't be reading this rant if I called it that.</p>
<p>Anyway, to demonstrate this, I wrote a version of the program that does exactly this using a simple divide and conquer approach (<a href="https://gitlab.com/AlphaOmegaProgrammer/stop-doing-data-synchronization-reference-repository/-/blob/master/04-divide-and-conquer.c" target="_blank">source here</a>). Yes, this actually counts up to 1 billion this time.</p>
<blockquote class="codeblock" data-language="stdout">
	<div>real&nbsp;&nbsp;&nbsp;&nbsp;0m0.609s</div>
	<div>user&nbsp;&nbsp;&nbsp;&nbsp;0m2.425s</div>
	<div>sys&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;0m0.001s</div>
</blockquote>
<p>As you can see, 4 threads is 4 times faster, which is exactly what we want. Although "just avoid race conditions bro" seems pretty obvious, this program is a trivial case that doesn't exist in the real world. I can't seriously be suggesting doing this for every program? Right?</p>
<p>...Right?</p>
<p>Ok no meme this time, but I am seriously suggesting that <em>most</em> programs can be written in a way that <em>greatly reduces</em> contention. But what causes contention? Contention happens every time a race condition would manifest, so what if we just make race conditions really unlikely?</p>
<h2 id="methodology">Methodology</h2>
<p>The start of any good methodology is to quantify and classify all possible possibilities. Let's start by organizing all possible programs into 2 different classes: programs that have all of the data upfront, and programs that receive data during execution. I don't think there's any other way to give a program data.</p>
<p>Let's start with the easier of the two classes: programs that have all of the data upfront. This scenario is quite trivial thanks to TLS. Huh? No, not Transport Layer Security, <a href="https://en.wikipedia.org/wiki/Thread-local_storage" target="_blank">Thread Local Storage</a>. Just like with the divide and conquer approach, each thread has its own isolated memory that it can work with. To be more methodical than the previous test, let's change the implementation slightly (<a href="https://gitlab.com/AlphaOmegaProgrammer/stop-doing-data-synchronization-reference-repository/-/blob/master/05-divide-and-conquer-formalized.c" target="_blank">source here</a>).</p>
<blockquote class="codeblock" data-language="stdout">
	<div>real&nbsp;&nbsp;&nbsp;&nbsp;0m0.698s</div>
	<div>user&nbsp;&nbsp;&nbsp;&nbsp;0m2.775s</div>
	<div>sys&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;0m0.002s</div>
</blockquote>
<p>The timings are a little bit slower because now we're touching memory and doing a bit of extra stuff, but you can see that the timings aren't significantly different from the previous test. Some variation of this pattern- Ew. No. Sorry for swearing. Some variation of this, uh, technique should be possible for any program in this class.</p>
<h2 id="writing-a-hashmap-in-c">Writing A Hashmap In C</h2>
<p>For the other class of problems, the first step is to make a hashmap in C. Don't worry though, because this hashmap implementation can be very simple. Our objective isn't necessarily to have O(1) time complexity, the goal is to have a bunch of isolated shared resources that the threads can interact with while having a low likelihood of contending with each other.</p>
<p>Network events are my go to example for these kinds of programs. In this case, socket file descriptors are just unsigned ints, so we can implement a simple hashmap by using some number of lowest bits from the socket fd as the "hash".</p>
<h2 id="ok-but-how-are-we-going-to-test-this">Ok, But How Are We Going To Test This?</h2>
<p>I can't really setup and use epoll in a quick and easy way, and anything involving networking is very hard to test in depth. Instead, let's start with writing a test that assumes the best case (no hash collisions, minimal contention) and test it using a bunch of different parameters (<a href="https://gitlab.com/AlphaOmegaProgrammer/stop-doing-data-synchronization-reference-repository/-/blob/master/06-hashmap.c" target="_blank">source here</a>).</p>
<h4 id="hashmap-single-thread-2-0">Hashmap - Single Thread - Hashmap Size 1 (2^0) (up to 1 billion)</h4>
<blockquote class="codeblock" data-language="stdout">
	<div>real&nbsp;&nbsp;&nbsp;&nbsp;0m16.085s</div>
	<div>user&nbsp;&nbsp;&nbsp;&nbsp;0m16.085s</div>
	<div>sys&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;0m0.000s</div>
</blockquote>
<p>As we can see, this is pretty comparable to atomics test from before with a single thread, but this is just doing the same thing in a more complex way. Let's add some more entries to the hashmap.</p>
<h4 id="hashmap-single-thread-2-12">Hashmap - Single Thread - Hashmap Size 4096 (2^12) (up to 1 billion)</h4>
<blockquote class="codeblock" data-language="stdout">
	<div>real&nbsp;&nbsp;&nbsp;&nbsp;0m16.379s</div>
	<div>user&nbsp;&nbsp;&nbsp;&nbsp;0m16.375s</div>
	<div>sys&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;0m0.003s</div>
</blockquote>
<h4 id="hashmap-single-thread-2-16">Hashmap - Single Thread - Hashmap Size 65536 (2^16) (up to 1 billion)</h4>
<blockquote class="codeblock" data-language="stdout">
	<div>real&nbsp;&nbsp;&nbsp;&nbsp;0m16.663s</div>
	<div>user&nbsp;&nbsp;&nbsp;&nbsp;0m16.661s</div>
	<div>sys&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;0m0.000s</div>
</blockquote>
<h4 id="hashmap-single-thread-2-24">Hashmap - Single Thread - Hashmap Size 16777216 (2^24) (up to 1 billion)</h4>
<blockquote class="codeblock" data-language="stdout">
	<div>real&nbsp;&nbsp;&nbsp;&nbsp;0m18.220s</div>
	<div>user&nbsp;&nbsp;&nbsp;&nbsp;0m18.140s</div>
	<div>sys&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;0m0.059s</div>
</blockquote>
<p>As we can see, there is a pretty small impact on performance even at massive hashmap sizes. Real world programs will likely have less of a performance hit, since this test iterates over the entire hashmap twice (once at the beginning to initialize the entry, and once at the end to tum the total). It's important to note that for a single thread on this test, the optimal hashmap size is 1 (quite obviously).</p>
<h4 id="hashmap-two-threads-2-0">Hashmap - Two Threads - Hashmap Size 1 (2^0) (up to 100 million)</h4>
<blockquote class="codeblock" data-language="stdout">
	<div>real&nbsp;&nbsp;&nbsp;&nbsp;0m16.003s</div>
	<div>user&nbsp;&nbsp;&nbsp;&nbsp;0m31.959s</div>
	<div>sys&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;0m0.001s</div>
</blockquote>
<h4 id="hashmap-two-threads-2-1">Hashmap - Two Threads - Hashmap Size 2 (2^1) (up to 100 million)</h4>
<blockquote class="codeblock" data-language="stdout">
	<div>real&nbsp;&nbsp;&nbsp;&nbsp;0m11.719s</div>
	<div>user&nbsp;&nbsp;&nbsp;&nbsp;0m23.361s</div>
	<div>sys&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;0m0.000s</div>
</blockquote>
<h4 id="hashmap-two-threads-2-12">Hashmap - Two Threads - Hashmap Size 4096 (2^12) (up to 1 billion)</h4>
<blockquote class="codeblock" data-language="stdout">
	<div>real&nbsp;&nbsp;&nbsp;&nbsp;0m9.325s</div>
	<div>user&nbsp;&nbsp;&nbsp;&nbsp;0m18.591s</div>
	<div>sys&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;0m0.004s</div>
</blockquote>
<h4 id="hashmap-two-threads-2-16">Hashmap - Two Threads - Hashmap Size 65536 (2^16) (up to 1 billion)</h4>
<blockquote class="codeblock" data-language="stdout">
	<div>real&nbsp;&nbsp;&nbsp;&nbsp;0m9.285s</div>
	<div>user&nbsp;&nbsp;&nbsp;&nbsp;0m18.413s</div>
	<div>sys&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;0m0.002s</div>
</blockquote>
<h4 id="hashmap-two-threads-2-24">Hashmap - Two Threads - Hashmap Size 16777216 (2^24) (up to 1 billion)</h4>
<blockquote class="codeblock" data-language="stdout">
	<div>real&nbsp;&nbsp;&nbsp;&nbsp;0m9.883s</div>
	<div>user&nbsp;&nbsp;&nbsp;&nbsp;0m19.049s</div>
	<div>sys&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;0m0.072s</div>
</blockquote>
<p>As we can see from the tests above, contention is very expensive, and the cost of a massive hashmap is far lower than the cost of contention. Another thing to note is that the optimal hashmap size for this test with 2 threads seems to be somewhere between 1024 and 2048.</p>
<h4 id="hashmap-four-threads-2-0">Hashmap - Four Threads - Hashmap size 1 (2^0) (up to 10 million)</h4>
<blockquote class="codeblock" data-language="stdout">
	<div>real&nbsp;&nbsp;&nbsp;&nbsp;0m2.872s</div>
	<div>user&nbsp;&nbsp;&nbsp;&nbsp;0m11.341s</div>
	<div>sys&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;0m0.001s</div>
</blockquote>
<h4 id="hashmap-four-threads-2-1">Hashmap - Four Threads - Hashmap size 2 (2^1) (up to 10 million)</h4>
<blockquote class="codeblock" data-language="stdout">
	<div>real&nbsp;&nbsp;&nbsp;&nbsp;0m2.538s</div>
	<div>user&nbsp;&nbsp;&nbsp;&nbsp;0m10.122s</div>
	<div>sys&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;0m0.002s</div>
</blockquote>
<h4 id="hashmap-four-threads-2-2">Hashmap - Four Threads - Hashmap size 4 (2^2) (up to 10 million)</h4>
<blockquote class="codeblock" data-language="stdout">
	<div>real&nbsp;&nbsp;&nbsp;&nbsp;0m1.793s</div>
	<div>user&nbsp;&nbsp;&nbsp;&nbsp;0m7.139s</div>
	<div>sys&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;0m0.000s</div>
</blockquote>
<h4 id="hashmap-four-threads-2-4">Hashmap - Four Threads - Hashmap size 16 (2^4) (up to 100 million)</h4>
<blockquote class="codeblock" data-language="stdout">
	<div>real&nbsp;&nbsp;&nbsp;&nbsp;0m9.467s</div>
	<div>user&nbsp;&nbsp;&nbsp;&nbsp;0m37.842s</div>
	<div>sys&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;0m0.001s</div>
</blockquote>
<h4 id="hashmap-four-threads-2-8">Hashmap - Four Threads - Hashmap size 256 (2^8) (up to 1 billion)</h4>
<blockquote class="codeblock" data-language="stdout">
	<div>real&nbsp;&nbsp;&nbsp;&nbsp;0m8.085s</div>
	<div>user&nbsp;&nbsp;&nbsp;&nbsp;0m32.256s</div>
	<div>sys&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;0m0.004s</div>
</blockquote>
<h4 id="hashmap-four-threads-2-12">Hashmap - Four Threads - Hashmap size 4096 (2^12) (up to 1 billion)</h4>
<blockquote class="codeblock" data-language="stdout">
	<div>real&nbsp;&nbsp;&nbsp;&nbsp;0m4.832s</div>
	<div>user&nbsp;&nbsp;&nbsp;&nbsp;0m19.287s</div>
	<div>sys&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;0m0.001s</div>
</blockquote>
<h4 id="hashmap-four-threads-2-16">Hashmap - Four Threads - Hashmap size 65536 (2^16) (up to 1 billion)</h4>
<blockquote class="codeblock" data-language="stdout">
	<div>real&nbsp;&nbsp;&nbsp;&nbsp;0m4.856s</div>
	<div>user&nbsp;&nbsp;&nbsp;&nbsp;0m19.325s</div>
	<div>sys&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;0m0.002s</div>
</blockquote>
<h4 id="hashmap-four-threads-2-24">Hashmap - Four Threads - Hashmap size 16777216 (2^24) (up to 1 billion)</h4>
<blockquote class="codeblock" data-language="stdout">
	<div>real&nbsp;&nbsp;&nbsp;&nbsp;0m5.0906s</div>
	<div>user&nbsp;&nbsp;&nbsp;&nbsp;0m18.892s</div>
	<div>sys&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;0m0.078s</div>
</blockquote>
<p>Again, we see that contention is very, very bad. The optimal hashmap size for 4 threads seems to be somewhere between 2048 and 4096.</p>
<h2 id="hashmap-takeaways">Hashmap Takeaways</h2>
<p>Now that we have a bunch of numbers, let's make the important observations. The optimal hashmap size seems to be somewhere around <code>1000 * number of threads</code>. Another important take away is that 1 thread took around <code>16 seconds</code> of real time and user time, two threads took around <code>9.3 seconds</code> real time with <code>18.4 seconds</code> of user time, and four threads took around <code>4.8 seconds</code> real time with around <code>19 seconds</code> of user time.</p>
<p>The user time differences when using threads indicate that there was still some amount of contention, but the more important real time measurements were roughly twice as fast for two threads and roughly 4 times as fast for 4 threads. Nothing in life is ever perfect, but these results are pretty good!</p>
<p>However, this is still the best case of a trivial example of something that's not even a hashmap. Let's look at something closer to a worse case scenario.</p>
<h2 id="hashmap-worst-case">Hashmap Worst Case</h2>
<p>Remember that hashmaps are backed by linked lists, and although there are numerous lock free data structures and algorithms, linked lists do not yet have a lock free implementation. This means that each hashmap entry needs to have a mutex to protect the linked list. Fortunately, rather than using a traditional mutex, we can just make a lock with atomics.</p>
<p>I won't post as many numbers as before, but it's time to run some tests (<a href="https://gitlab.com/AlphaOmegaProgrammer/stop-doing-data-synchronization-reference-repository/-/blob/master/07-hashmap-forced-contention.c?ref_type=heads" target="_blank">source here</a>).</p>
<h4 id="hashmap-worst-case-single-thread-2-0">Hashmap Worst Case - Single Thread - Hashmap Size 1 (2^0) (up to 1 billion)</h4>
<blockquote class="codeblock" data-language="stdout">
	<div>real&nbsp;&nbsp;&nbsp;&nbsp;0m30.079s</div>
	<div>user&nbsp;&nbsp;&nbsp;&nbsp;0m30.066s</div>
	<div>sys&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;0m0.012s</div>
</blockquote>
<p>Right away, this test is about twice as slow as the previous test, but that's fine because this test does more than the previous test.</p>
<h4 id="hashmap-worst-case-single-thread-2-16">Hashmap Worst Case - Single Thread - Hashmap Size 65536 (2^16) (up to 1 billion)</h4>
<blockquote class="codeblock" data-language="stdout">
	<div>real&nbsp;&nbsp;&nbsp;&nbsp;0m35.559s</div>
	<div>user&nbsp;&nbsp;&nbsp;&nbsp;0m35.548s</div>
	<div>sys&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;0m0.009s</div>
</blockquote>
<p>And this is why testing so many different things is important. With a single hashmap entry, the CPU was just cashing that entry, which sped things up a lot compared to our next tests. Linked lists are not cache friendly.</p>
<h4 id="hashmap-worst-case-two-threads-2-16">Hashmap Worst Case - Two Threads - Hashmap Size 65536 (2^16) (up to 1 billion)</h4>
<blockquote class="codeblock" data-language="stdout">
	<div>real&nbsp;&nbsp;&nbsp;&nbsp;0m23.641s</div>
	<div>user&nbsp;&nbsp;&nbsp;&nbsp;0m47.256s</div>
	<div>sys&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;0m0.016s</div>
</blockquote>
<h4 id="hashmap-worst-case-four-threads-2-16">Hashmap Worst Case - Four Threads - Hashmap Size 65536 (2^16) (up to 1 billion)</h4>
<blockquote class="codeblock" data-language="stdout">
	<div>real&nbsp;&nbsp;&nbsp;&nbsp;0m17.719s</div>
	<div>user&nbsp;&nbsp;&nbsp;&nbsp;1m10.834s</div>
	<div>sys&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;0m0.016s</div>
</blockquote>
<p>Now let's look at the numbers. A single thread is <code>35.5 seconds</code> for both real and user time. Two threads took around <code>23.5 seconds</code> real time and <code>47.2 seconds</code> user time, and four threads took around <code>17.7 seconds</code> real time and <code>70.8 seconds</code> user time.</p>
<p>The real time is indeed shrinking, a bit slower than we'd like, but remember that this is simulating the worst case scenario, so the fact that this isn't atrocious is already a W. Looking at the user time, we can see that the user time grows in the way we'd expect. This is good though because we can tell that each CPU core was being utilized fully, and we expect (and see) a lot of contention.</p>
<h2 id="improving-the-worst-case">Improving The Worst Case</h2>
<p>So one obvious improvement for these tests is that each thread is starting at the beginning of the hashmap and going through sequentially rather than accessing elements at random, so the real world will likely show better results than what we saw here. I can't think of an obvious and easy way to test this, but this is pretty obvious that it'd be an improvement.</p>
<p>The other obvious improvement is backing off during contention. How is this done? Usually the thread just does a <code>sleep(0)</code> to tell the OS to context switch away to another thread. The idea is that mutexes are faster than atomics under heavy contention, so under heavy contention just do what a mutex does. One less thread contending for the resource will make it easier for the other threads to make forward progress, and then the sleeping thread will come back later when there is hopefully less contention. There are even different algorithms to use to figure out when to back off, there's linear backoff, exponential backoff...</p>
<p>But doing any kind of backoff at all is wasting CPU cycles because of context switching, and decreases throughput. In fact, talking about backoff is what made me want to do this rant in the first place. Finally, we're at the good stuff.</p>
<h2 id="wake-up">WAKE UP</h2>
<p>Ok, so think about this. Instead of sleeping during resource contention, what if the thread actually did something useful during resource contention? There are 2 approaches I've thought of, but sadly writing test programs for each of these approaches would be an astronomical amount of work, so I will leave this as an exercise to the reader.</p>
<h2 id="backoff-alterantive-1-track-state-of-processing">Backoff Alternative 1 - Track State of Processing</h2>
<p>The first approach is something I've done and is something I can anecdotally say is effective. The first approach is, for each piece of potentially contentious data, use one of the three following states to track the processing of that piece of data: <code>inactive</code>, <code>processing</code>, and <code>keep_processing</code>. The data is initialized in the <code>inactive</code> state.</p>
<p>When some activity occurs that's related to a piece of data, <strong>do not read the activity information</strong>. Instead, the thread immediately finds the associated data and reads the current processing state for the data.</p>
<ul>
	<li>If the state is <code>inactive</code>, the thread will attempt to compare exchange the state with <code>processing</code>, and upon success the thread is now responsible for processing that event and its related data.</li>
	<li>If the state is <code>processing</code>, the thread will attempt to compare exchange the state with <code>keep_processing</code>, and upon success the thread goes back to waiting for another event to occur.</li>
	<li>If the state is <code>keep_processing</code>, the thread simply goes back to waiting for another event to occur.</li>
	<li>If updating the state fails, go through this process again.</li>
</ul>
<p>When the processing thread goes back to update the processing state for the potentially contentious data:</p>
<ul>
	<li>If the state is <code>keep_processing</code>, the thread will set the state to <code>processing</code> and then attempt to process events related to the data again.</li>
	<li>If the state is <code>processing</code>, the thread will attempt to compare exchange the state to <code>inactive</code>, and upon success the thread goes back to waiting for another event to occur.</li>
	<li>If updating the state fails, go through this process again.</li>
</ul>
<p>The great thing about this technique is that the threads that are waiting for events count the state up, and the thread that is processing the event and data counts the state down. Since there is nothing above the <code>keep_processing</code> state, this ensures that no thread will ever have to do more than 3 atomic compare exchanges, barring some very poorly timed context switching.</p>
<h2 id="backoff-alterantive-2-try-again-later">Backoff Alternative 2 - Try Again Later</h2>
<p>Just have the thread hold ownership of the data and little bit longer, and go off and do other work before coming back and trying the atomic operation again. Think of async. This approach requires structuring your program in a specific way though and would likely be difficult to retrofit into an existing program. However I think this approach would have optimal performance.</p>
<h2 id="conclusion">Conclusion</h2>
<p>Threading is hard, and threading fast is even harder. I don't know all of the answers, and I'm certain someone smarter than me will come along and figure out a better way to minimize overhead in multithreaded programs.</p>
<p>However, the point of this rant isn't for me to pretend I know everything or to speculate that these suggestions are the best possible implementations. The point is that I see a problem, and a solution, and I wanted to share that knowledge and hopefully inspire others to think more critically about threading fast. Feel free to take the example programs from <a href="https://gitlab.com/AlphaOmegaProgrammer/stop-doing-data-synchronization-reference-repository" target="_blank">the repository</a> and play with them. Experiment, Learn. Maybe you'll figure out something great and write a rant about how I'm stupid.</p>
<p>Happy Coding,</p>
<p>AOP</p>
