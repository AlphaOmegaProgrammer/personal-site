.PHONY: clean all

all:
	./build.sh

clean:
	-find public -delete
